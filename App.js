import {NavigationContainer} from "@react-navigation/native";
import Route from "./src/route/Route";
import 'react-native-gesture-handler';

export default function App() {
  return (
    <NavigationContainer>
        <Route />
    </NavigationContainer>
  );
}