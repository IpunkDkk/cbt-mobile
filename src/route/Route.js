import {createStackNavigator} from "@react-navigation/stack";
import OptionLogin from "../pages/OptionLogin";
import About from "../pages/About";
import Login from "../pages/Login";
import SignUp from "../pages/SignUp";

const Stack = createStackNavigator()

const Route = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen name={'OptionLogin'} component={OptionLogin} options={{headerShown:false}}/>
            <Stack.Screen name={'Login'} component={Login} options={{headerShown:false}} />
            <Stack.Screen name={'SignUp'} component={SignUp} options={{headerShown: false}} />
            <Stack.Screen name={'About'} component={About} options={{headerShown:false}} />

        </Stack.Navigator>
    )
}

export default Route;