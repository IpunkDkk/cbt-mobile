import {Text, TouchableOpacity, View} from "react-native";
import {useNavigation} from "@react-navigation/native";

const About = () => {
    const navigation = useNavigation()
    return(
        <View style={{flex: 1 , justifyContent:'center', alignItems:'center'}}>
            <TouchableOpacity onPress={() => {navigation.navigate('OptionLogin')}}>
                <Text>Masuk</Text>
            </TouchableOpacity>
        </View>
    )
}

export default About;