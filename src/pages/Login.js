import {Component} from "react";
import {Alert, Button, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import asyncStorage from "@react-native-async-storage/async-storage/src/AsyncStorage";
import { TextInput } from 'react-native-paper';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

class Login extends Component {
    state = {
        email : '',
        password : '',
        login : '',
        secure: true,
    }
    componentDidMount() {
        this.handleKey()
    }

    handleKey = async () => {
        let data =await asyncStorage.getItem('login')
        this.setState({login:data})
    }

    emailText = inputText => {
        this.setState({email: inputText})
    }
    passwordText = inputText => {
        this.setState({password : inputText})
    }
    handleCLick = async () => {
        Alert.alert('Messege', "username" + this.state.password )
    }

    testF = () => {
        this.props.navigation.navigate('OptionLogin')
    }

    render() {
        const navigation = this.props
        return(
            <View style={style.container}>
                <View style={{width:'100%'}} >
                    <View style={{marginBottom:30}}>
                        <Text style={style.textHeader} >Welcome Back</Text>
                        <Text style={style.text}>Sign In And Get Started</Text>
                    </View>
                    <View style={style.input}>
                        <TextInput  placeholder={"Email"} mode={"outlined"} onChangeText={this.emailText} />
                    </View>
                    <View style={style.input}>
                        <TextInput secureTextEntry={this.state.secure}
                                   // style={styles.textInputStyle}
                                   right={
                                       <TextInput.Icon
                                           name={() => <MaterialCommunityIcons name={this.state.secure ? "eye-off" : "eye"} size={28} color={'black'} />} // where <Icon /> is any component from vector-icons or anything else
                                           onPress={() => { this.state.secure ? this.setState({secure:false}) : this.setState({secure:true}) }}
                                       />
                                   }
                                   placeholder={"Password"}
                                   underlineColorAndroid="transparent"
                                   mode={"outlined"}
                                   onChangeText={this.passwordText}
                        />
                    </View>
                    <View style={style.forget} >
                        <TouchableOpacity>
                            <Text style={{fontSize:12}}>Forget Password</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={style.button}>
                        <TouchableOpacity onPress={() => {this.handleCLick()}} >
                            <Text style={{color:'white'}}>Sign In</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{alignItems:'center', marginTop:12}}>
                        <TouchableOpacity onPress={() => {this.props.navigation.navigate('SignUp')}}>
                            <Text style={{fontSize:12}}>Dont have an account? signup</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{alignItems:'center', marginTop:12}}>
                        <TouchableOpacity onPress={() => {this.testF()}}>
                            <Text style={{fontSize:12, color:'#8403fc'}}>Not a {this.state.login}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

export default Login;

const style = StyleSheet.create({
    container : {
        flex:1,
        justifyContent:'center',
        paddingHorizontal:10
    },
    input : {
        // color:'black',
        // marginBottom:12,
        // borderWidth:1,
        // padding:20,
        // paddingVertical:10,
        // borderRadius:5,
    },
    button : {
        backgroundColor:'#8403fc',
        paddingVertical:10,
        borderRadius:5,
        marginTop:20,
        alignItems:'center'
    },
    forget :{
        alignItems: 'flex-end'
    },
    textHeader:{
        color: '#8403fc',
        fontSize : 24,
        marginBottom:10,
        fontFamily:'Roboto',
    },
    text:{
        color:'black',
        marginBottom:30,
        fontFamily:'Roboto',

    }
});