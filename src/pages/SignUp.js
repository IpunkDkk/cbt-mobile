import {Component} from "react";
import {Alert, Button, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import asyncStorage from "@react-native-async-storage/async-storage/src/AsyncStorage";

class SignUp extends Component {
    state = {
        fistname: '',
        lastname: '',
        username: '',
        email : '',
        password : '',
        login : '',
    }
    componentDidMount() {
    }

    fistnameText = inputText => {
        this.setState({email: inputText})
    }
    lastnameText = inputText => {
        this.setState({email: inputText})
    }
    usenameText = inputText => {
        this.setState({email: inputText})
    }
    emailText = inputText => {
        this.setState({email: inputText})
    }
    passwordText = inputText => {
        this.setState({password : inputText})
    }
    handleCLick = async () => {
        Alert.alert('Messege', "username" + this.state.password )
    }

    render() {
        const navigation = this.props
        return(
            <View style={style.container}>
                <View style={{width:'100%'}} >
                    <View style={{marginBottom:30}}>
                        <Text style={style.textHeader} >Sign Up</Text>
                        <Text style={style.text}>Welcome To the examination portal of school</Text>
                    </View>
                    <View style={style.input}>
                        <TextInput  placeholder={"Fistname"} onChangeText={this.fistnameText} />
                    </View>
                    <View style={style.input}>
                        <TextInput  placeholder={"Lastname"} onChangeText={this.lastnameText} />
                    </View>
                    <View style={style.input}>
                        <TextInput  placeholder={"Email"} onChangeText={this.usenameText} />
                    </View>
                    <View style={style.input}>
                        <TextInput  placeholder={"Email"} onChangeText={this.emailText} />
                    </View>
                    <View style={style.input}>
                        <TextInput secureTextEntry={true} placeholder={"Password"} onChangeText={this.passwordText} />
                    </View>
                    <View style={style.button}>
                        <TouchableOpacity onPress={() => {this.handleCLick()}} >
                            <Text style={{color:'white'}}>Sign In</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{alignItems:'center', marginTop:12}}>
                        <TouchableOpacity onPress={() => {this.props.navigation.navigate('Login')}}>
                            <Text style={{fontSize:12}}>Already have an account? Log In</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

export default SignUp;

const style = StyleSheet.create({
    container : {
        flex:1,
        justifyContent:'center',
        paddingHorizontal:10
    },
    input : {
        color:'black',
        marginBottom:12,
        borderWidth:1,
        padding:20,
        paddingVertical:10,
        borderRadius:5,
    },
    button : {
        backgroundColor:'#8403fc',
        paddingVertical:10,
        borderRadius:5,
        marginTop:20,
        alignItems:'center'
    },
    forget :{
        alignItems: 'flex-end'
    },
    textHeader:{
        color: '#8403fc',
        fontSize : 24,
        marginBottom:10,
        fontFamily:'Roboto',
    },
    text:{
        color:'black',
        marginBottom:30,
        fontFamily:'Roboto',

    }
});