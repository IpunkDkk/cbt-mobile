import {Alert, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useEffect, useState} from "react";
import useEvent from "react-native-web/dist/modules/useEvent";
import {useNavigation} from "@react-navigation/native";
import asyncStorage from "@react-native-async-storage/async-storage/src/AsyncStorage";

export default function OptionLogin() {
    const [warna, setwarna] = useState({
            'bg_color_a' : '#8403fc',
            'bd_color_a' : '#8403fc',
            'f_color_a' : 'white',
            'bg_color' : 'white',
            'bd_color' : '#8403fc',
            'f_color' : '#8403fc',
    })
    const navigation = useNavigation()
    const HandleColor = (a,b) => {
        if (a === true){
            if (b === false){
                let config = {
                    'bg_color_a' : '#8403fc',
                    'bd_color_a' : '#8403fc',
                    'f_color_a' : 'white',
                    'bg_color' : 'white',
                    'bd_color' : '#8403fc',
                    'f_color' : '#8403fc',
                }
                setwarna(config)
            }

        }else if(b === true){
            if(a === false){
                let config = {
                    'bg_color' : '#8403fc',
                    'bd_color' : '#8403fc',
                    'f_color' : 'white',
                    'bg_color_a' : 'white',
                    'bd_color_a' : '#8403fc',
                    'f_color_a' : '#8403fc',
                }
                setwarna(config)
            }
        }
    }
    const HandleButton = data => {
        if(data === 'tombol1'){
            let tombol1 = true
            let tombol2 = false
            HandleColor(tombol1, tombol2)
            asyncStorage.setItem('login','teacher');
        }else{
            let tombol1 = false
            let tombol2 = true
            HandleColor(tombol1, tombol2)
            asyncStorage.setItem('login','student');
        }

    }

    return (
        <View style={styles.container}>
            <View style={{width:'100%'}}>
                <TouchableOpacity style={{
                    marginTop: 12,
                    backgroundColor: warna.bg_color_a,
                    borderWidth: 2, //border
                    borderColor: warna.bd_color_a,
                    paddingVertical: 15,
                    borderRadius: 10,
                    alignItems: 'center'
                }} onPress={()=> {HandleButton('tombol1')}} >
                    <Text style={{color:warna.f_color_a}} >I am Teacher</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{
                    marginTop:12,
                    backgroundColor:warna.bg_color,
                    borderWidth:2,
                    borderColor:warna.bd_color,
                    paddingVertical:15,
                    borderRadius:10,
                    alignItems:'center'
                }} onPress={()=> {HandleButton('tombol2')}} >
                    <Text style={{color:warna.f_color}} >I am student</Text>
                </TouchableOpacity>
            </View>
            <View style={{width:'100%', marginTop:100, flexDirection:'row'}}>
                <TouchableOpacity style={styles.tombol3} onPress={() => {navigation.navigate('SignUp')}} >
                    <Text style={{color:'white'}} >SignUp</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.tombol4} onPress={() => {navigation.navigate('Login') }} >
                    <Text style={{color:'#8403fc'}} >Login</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10 //jarak dari kulit
    },
    tombol3: {
        flex: 1,
        marginHorizontal:5,
        paddingVertical:10,
        borderRadius:5,
        borderColor:'#8403fc',
        borderWidth:2,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#8403fc'

    },
    tombol4:{
        flex: 1,
        marginHorizontal:5,
        paddingVertical:10,
        borderRadius:5,
        borderColor:'#8403fc',
        borderWidth:2,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: 'white'
    }
});
